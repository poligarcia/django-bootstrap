from django.shortcuts import render


def home(request):
    """View function for home page of site."""

    return render(request, 'home.html')

def home_again(request):
    """View function for home page of site."""

    raise Exception("Oh no! This is an unexpected error!!!")
